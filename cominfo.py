# -*- coding: utf-8 -*-

# @Time    : 2021/7/4 22:37
# @Author  : Zerlaer
# @Blog   : zerlaer.com
# @File    : cominfo.py
# @Software: PyCharm
import os
import socket
import uuid
import wmi
import xlrd
import xlwt
from gooey import Gooey, GooeyParser
from xlutils.copy import copy


def os_version():
    c = wmi.WMI()
    # 获取操作系统版本
    for sys in c.Win32_OperatingSystem():
        return sys.Caption


def os_type():
    # 系统类型
    c = wmi.WMI()
    for sys in c.Win32_OperatingSystem():
        return sys.OSArchitecture


def cpu_type():
    # CPU类型
    c = wmi.WMI()
    for processor in c.Win32_Processor():
        return processor.Name.strip()


def memory_size():
    # 内存大小
    c = wmi.WMI()
    for Memory in c.Win32_PhysicalMemory():
        return "%.fGB" % (int(Memory.Capacity) / 1048576 / 1024)


def get_hostname():
    # 主机名
    hostname = socket.gethostname()
    return hostname


def get_ip():
    # ip地址
    hostname = socket.gethostname()
    ip = socket.gethostbyname(hostname)
    return ip


def get_mac():
    # MAC地址
    addr = hex(uuid.getnode())[2:].upper()
    addr = '-'.join(addr[i:i + 2] for i in range(0, len(addr), 2))
    return addr


@Gooey(language='chinese', program_name="主机信息获取工具", default_size=(720, 680), required_cols=2,
    encoding="utf-8",
    menu=[{
        'name': '关于',
        'items': [{
            'type': 'AboutDialog',
            'menuTitle': '关于',
            'name': '主机信息获取工具',
            'description': '自动获取当前用户电脑信息并保存至Excel表',
            'version': '1.0.0',
            'copyright': '2021',
            'website': 'https://gitlab.com/zerlaer/cominfo',
            'developer': 'Zerlaer',
            'license': 'MIT'
        }, {
            'type': 'MessageDialog',
            'menuTitle': '提示',
            'caption': '提示信息',
            'message': '输入自己的用户信息后点击提交自动追加保存至Excel表'
        }, ]
    }, {
        'name': '作者',
        'items': [{
            'type': 'Link',
            'menuTitle': '博客',
            'url': 'https://zerlaer.com'
        }]
    }]


)
def main():
    msg = '主机信息获取工具'
    parser = GooeyParser(description=msg)
    parser.add_argument("name", metavar='员工姓名', help="请输入中文名")
    parser.add_argument("position", metavar='所在岗位', help="请输入岗位名称")
    parser.add_argument("department", metavar='所在部门', help="请输入部门名称")
    parser.add_argument("version", metavar='系统版本', default=os_version())
    parser.add_argument("type", metavar='系统类型', default=os_type())
    parser.add_argument("cpu", metavar='CPU类型', default=cpu_type())
    parser.add_argument("memory", metavar='内存大小', default=memory_size())
    parser.add_argument("hostname", metavar='主机名', default=get_hostname())
    parser.add_argument("mac", metavar='MAC地址', default=get_mac())
    parser.add_argument("ip", metavar='IP地址', default=get_ip())

    args = parser.parse_args()
    path = 'data.xls'
    sheet_name = 'data'
    title = [["员工姓名", "所在岗位", "所在部门", "系统版本", "系统类型", "CPU类型", "内存大小", "主机名", "MAC地址", "IP地址"]]
    value = [
        [args.name, args.position, args.department, args.version, args.type, args.cpu, args.memory, args.hostname,
         args.mac, args.ip]]

    def write_excel_xls(path, sheet_name, value):
        index = len(value)  # 获取需要写入数据的行数
        workbook = xlwt.Workbook()  # 新建一个工作簿
        sheet = workbook.add_sheet(sheet_name)  # 在工作簿中新建一个表格
        for i in range(0, index):
            for j in range(0, len(value[i])):
                sheet.write(i, j, value[i][j])  # 像表格中写入数据（对应的行和列）
        workbook.save(path)  # 保存工作簿
        print("表格创建成功！")

    def write_excel_xls_append(path, value):
        index = len(value)  # 获取需要写入数据的行数
        workbook = xlrd.open_workbook(path)  # 打开工作簿
        sheets = workbook.sheet_names()  # 获取工作簿中的所有表格
        worksheet = workbook.sheet_by_name(sheets[0])  # 获取工作簿中所有表格中的的第一个表格
        rows_old = worksheet.nrows  # 获取表格中已存在的数据的行数
        new_workbook = copy(workbook)  # 将xlrd对象拷贝转化为xlwt对象
        new_worksheet = new_workbook.get_sheet(0)  # 获取转化后工作簿中的第一个表格
        for i in range(0, index):
            for j in range(0, len(value[i])):
                new_worksheet.write(i + rows_old, j, value[i][j])  # 追加写入数据，注意是从i+rows_old行开始写入
        new_workbook.save(path)  # 保存工作簿
        print("表格追加写入数据成功！")

    if os.path.exists(path):
        write_excel_xls_append(path, value)
    else:
        write_excel_xls(path, sheet_name, title)
        write_excel_xls_append(path, value)


if __name__ == '__main__':
    main()
